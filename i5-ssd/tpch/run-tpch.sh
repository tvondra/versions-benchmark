#!/bin/bash

DATA="/mnt/raid/bench-data"
PSQL=/var/lib/postgresql/builds/12/bin/psql

PATH_OLD=$PATH

RESULTS=$1

RUNS=2
QUERY_TIMEOUT=900000	# 15 minutes by default

# kill all collect-stats instances that might be running and cleanup
ps ax | grep collect-stats | grep -v grep | awk '{print $1}' | xargs kill > /dev/null 2>&1
rm -f pgbench_log.*

function log_step {
	echo `date +%s` [`date +%d/%m/%Y` `date +'%I:%M:%S %p'`] $1 >> $2
}

function reset_sar_data {
	# remove old sar data and sleep for 60 secs to make sure we
	# get data from the beginning
	sudo rm -f /var/log/sa/*
	sleep 60
}

function dump_sar_data {

	mkdir $1/sar

	cp /var/log/sa/sa* $1/sar/

	for f in $1/sar/sa*; do

		# paging stats
		sar -f $f -B > $f.paging

		# I/O transfer rate stats
		sar -f $f -b > $f.io_transfer

		# I/O device stats
		sar -f $f -d -p > $f.devices

		# CPU stats
		sar -f $f -P ALL > $f.cpus

		# queue length
		sar -f $f -q > $f.run_queue

		# memory utilization
		sar -f $f -r ALL > $f.memory_util

		# swap
		sar -f $f -S > $f.swap

		# CPU utilization
		sar -f $f -u ALL > $f.cpu_util

		# swapping
		sar -f $f -W > $f.swapping

		# task creation
		sar -f $f -w > $f.tasks

	done

}

for s in 32; do
# for s in 1 8 32; do

	for m in noparallel parallel; do

		for v in 8.3 12 9.4 9.1 10 9.0 8.4 9.2 11 9.5 9.3 9.6; do

			# if there's no config for this mode / version, skip it
			if [ ! -f "config/$v/$m.conf" ]; then
				continue
			fi

			mkdir -p $RESULTS/$m/$s/$v

			# set the correct PATH
			PATH=/var/lib/postgresql/builds/$v/bin:$PATH_OLD

			echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "terminating old cluster"

			# kill and remove the old cluster
			killall -9 postgres > /dev/null 2>&1
			rm -Rf $DATA

			echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "initializing new cluster"

			log_step "cluster init" $RESULTS/$m/$s/$v/steps.log

			# initialize a new cluster
			initdb -D $DATA > $RESULTS/$m/$s/$v/init.log 2>&1

			# copy the configuration file
			cp config/$v/$m.conf $DATA/postgresql.conf

			# start the cluster
			pg_ctl -D $DATA -l $RESULTS/$m/$s/$v/pg.log start > $RESULTS/$m/$s/$v/start.log 2>&1

			# wait for the cluster to actually start
			sleep 10;

			echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "initializing tpch database"

			# initialize the database
			log_step "tpch createdb" $RESULTS/$m/$s/$v/steps.log
			createdb test > $RESULTS/$m/$s/$v/createdb.log 2>&1

			# remove old sar data
			reset_sar_data

			mkdir -p $RESULTS/$m/$s/$v/load

			./collect-stats.sh $RESULTS/$m/$s/$v/load $v &
			stats_pid=$!

			# create the tables
			log_step "tpch create tables" $RESULTS/$m/$s/$v/load/steps.log
			$PSQL test < ddl/create.sql > $RESULTS/$m/$s/$v/load/tpch.create.log 2>&1

			# load data into tables
			load_start=`date +%s`
			log_step "tpch load data" $RESULTS/$m/$s/$v/load/steps.log
			sed "s|CSVDIR|/mnt/data/tpch/$s|" ddl/load.sql > /tmp/load.sql 2>&1
			$PSQL test < /tmp/load.sql > $RESULTS/$m/$s/$v/load/tpch.load.log 2>&1
			load_end=`date +%s`

			echo "create $load_start $load_end" >> $RESULTS/$m/$s/$v/load/steps.csv 2>&1

			# create primary keys
			pkeys_start=`date +%s`
			log_step "tpch create primary keys" $RESULTS/$m/$s/$v/load/steps.log
			$PSQL test < ddl/pkeys.sql > $RESULTS/$m/$s/$v/load/tpch.pkeys.log 2>&1
			pkeys_end=`date +%s`

			echo "pkeys $pkeys_start $pkeys_end" >> $RESULTS/$m/$s/$v/load/steps.csv 2>&1

			# create indexes keys
			indexes_start=`date +%s`
			log_step "tpch create indexes" $RESULTS/$m/$s/$v/load/steps.log
			$PSQL test < ddl/indexes.sql > $RESULTS/$m/$s/$v/load/tpch.indexes.log 2>&1
			indexes_end=`date +%s`

			echo "indexes $indexes_start $indexes_end" >> $RESULTS/$m/$s/$v/load/steps.csv 2>&1

			# create foreign keys
			alter_start=`date +%s`
			log_step "tpch create foreign keys" $RESULTS/$m/$s/$v/load/steps.log
			$PSQL test < ddl/alter.sql > $RESULTS/$m/$s/$v/load/tpch.alter.log 2>&1
			alter_end=`date +%s`

			echo "fkeys $alter_start $alter_end" >> $RESULTS/$m/$s/$v/load/steps.csv 2>&1

			echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "vacuum tpch database"

			log_step "vacuum analyze" $RESULTS/$m/$s/$v/load/steps.log

			psql test -c "vacuum analyze" > $RESULTS/$m/$s/$v/load/vacuum.log 2>&1

			echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "checkpoint tpch database"

			log_step "checkpoint" $RESULTS/$m/$s/$v/load/steps.log

			psql test -c "checkpoint" > $RESULTS/$m/$s/$v/load/checkpoint.log 2>&1

			kill $stats_pid > /dev/null 2>&1

			$PSQL test -c "CREATE TABLE query_timing (start_time timestamptz, end_time timestamptz)" > /dev/null 2>&1

			# run queries
			for q in `seq 1 22`; do

				# drop caches and restart the cluster, so that the first run
				# starts with empty caches
				sudo ./drop-caches.sh > /dev/null 2>&1
				pg_ctl -D $DATA -l $RESULTS/$m/$s/$v/pg.log restart >> $RESULTS/$m/$s/$v/start.log 2>&1

				# sleep for a while, so that the cluster actually starts
				sleep 10

				echo "========== query $q ==========" >> $RESULTS/$m/$s/$v/explain.log 2>&1

				$PSQL test >> $RESULTS/$m/$s/$v/explain.log 2>&1 <<EOF
SET statement_timeout = $QUERY_TIMEOUT;
\i explain/$q.sql
EOF

				mkdir -p $RESULTS/$m/$s/$v/queries/$q

				# start collection of sar data (only once for all runs)
				./collect-stats.sh $RESULTS/$m/$s/$v/queries/$q $v &
				stats_pid=$!

				for r in `seq 1 $RUNS`; do

					echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "query $q run $r timeout" $QUERY_TIMEOUT

					log_step "query $q run $r" $RESULTS/$m/$s/$v/steps.log

					start_time=`date +%s`

					$PSQL test >> $RESULTS/$m/$s/$v/queries/$q/run.log 2>&1 <<EOF
SET statement_timeout = $QUERY_TIMEOUT;
\o /dev/null;
TRUNCATE query_timing;
BEGIN;
INSERT INTO query_timing (start_time) VALUES (clock_timestamp());
\i queries/$q.sql
UPDATE query_timing SET end_time = clock_timestamp();
COMMIT;
EOF

					end_time=`date +%s`

					# determine query duration
					t=`$PSQL test -t -A -c "SELECT extract(epoch from end_time) - extract(epoch from start_time) FROM query_timing"`

					log_step "query $q run $r done $t" $RESULTS/$m/$s/$v/steps.log

					echo $q $r $t $start_time $end_time >> $RESULTS/$m/$s/$v/queries.csv 2>&1

				done

				kill $stats_pid > /dev/null 2>&1

				echo "========== query $q ==========" >> $RESULTS/$m/$s/$v/explain-analyze.log 2>&1

				$PSQL test >> $RESULTS/$m/$s/$v/explain-analyze.log 2>&1 <<EOF
SET statement_timeout = $QUERY_TIMEOUT;
\i explain-analyze/$q.sql
EOF

			done

			dump_sar_data $RESULTS/$m/$s/$v

			log_step "done" $RESULTS/$m/$s/$v/steps.log

		done;

	done

done;

