COPY part FROM 'CSVDIR/part.csv' WITH CSV DELIMITER '|';
COPY region FROM 'CSVDIR/region.csv' WITH CSV DELIMITER '|';
COPY nation FROM 'CSVDIR/nation.csv' WITH CSV DELIMITER '|';
COPY supplier FROM 'CSVDIR/supplier.csv' WITH CSV DELIMITER '|';
COPY customer FROM 'CSVDIR/customer.csv' WITH CSV DELIMITER '|';
COPY partsupp FROM 'CSVDIR/partsupp.csv' WITH CSV DELIMITER '|';
COPY orders FROM 'CSVDIR/orders.csv' WITH CSV DELIMITER '|';
COPY lineitem FROM 'CSVDIR/lineitem.csv' WITH CSV DELIMITER '|';
