#!/usr/bin/bash

DIR=$1

dropdb --if-exists pgbench_log > /dev/null 2>&1
createdb pgbench_log > /dev/null 2>&1

psql pgbench_log -c "CREATE TABLE pgbench_log (interval_start bigint, num_transactions bigint, sum_latency bigint, sum_latency_2 bigint, min_latency bigint, max_latency bigint)" > /dev/null 2>&1

for f in $DIR/pgbench_log.*; do
	cat $f | psql pgbench_log -c "COPY pgbench_log FROM stdin WITH (DELIMITER ' ')" > /dev/null 2>&1
	rm $f
done

psql pgbench_log -c "analyze pgbench_log" > /dev/null 2>&1

psql pgbench_log -c "COPY (SELECT interval_start, sum(num_transactions) AS num_transactions, sum(sum_latency) AS sum_latency, sum(sum_latency_2) AS sum_latency_2, min(min_latency) AS min_latency, max(max_latency) AS max_latency FROM pgbench_log GROUP BY interval_start ORDER BY interval_start) TO stdout" > $DIR/pgbench_log.csv 2>&1
