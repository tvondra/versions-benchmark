#!/usr/bin/bash

DIR=$1

for x in $DIR/*; do

	machine=`basename $x`

	# skip dirs without pgbench results
	if [ ! -d $DIR/$machine/tpch ]; then
		continue
	fi

	for mode in noparallel parallel; do

		for s in $DIR/$machine/tpch/results/$mode/*; do

			scale=`basename $s`

			for v in $DIR/$machine/tpch/results/$mode/$scale/*; do

				version=`basename $v`

				sed "s/^/$mode $scale $version /" $DIR/$machine/tpch/results/$mode/$scale/$version/queries.csv

			done

		done

	done

done
