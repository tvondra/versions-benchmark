#!/usr/bin/bash

DIR=$1

for x in $DIR/*; do

	machine=`basename $x`

	# skip dirs without pgbench results
	if [ ! -d $DIR/$machine/pgbench ]; then
		continue
	fi

	for type in ro rw; do

		for s in $DIR/$machine/pgbench/results-$type/*; do

			scale=`basename $s`

			for v in $DIR/$machine/pgbench/results-$type/$scale/*; do

				version=`basename $v`

				for mode in simple prepared; do

					for c in $DIR/$machine/pgbench/results-$type/$scale/$version/$mode/*; do

						clients=`basename $c`

						for r in $DIR/$machine/pgbench/results-$type/$scale/$version/$mode/$clients/*; do

							run=`basename $r`

							tps=`gunzip -c $DIR/$machine/pgbench/results-$type/$scale/$version/$mode/$clients/$run/pgbench.log.gz | grep excluding | awk '{print $3}'`

							echo $machine $type $scale $version $mode $clients $run $tps

						done

					done

				done

			done

		done

	done

done
