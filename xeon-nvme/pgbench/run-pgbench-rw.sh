#!/bin/bash

DATA="/mnt/data/bench-data"

PATH_OLD=$PATH

RESULTS=$1
PGBENCH=/var/lib/postgresql/builds/12/bin/pgbench

RUNS=2
DURATION_WARMUP=600
DURATION_TEST=3600

# kill all collect-stats instances that might be running and cleanup
ps ax | grep collect-stats | grep -v grep | awk '{print $1}' | xargs kill
rm pgbench_log.*

function log_step {
	echo `date +%s` [`date +%d/%m/%Y` `date +'%I:%M:%S %p'`] $1 >> $2
}

function reset_sar_data {
	# remove old sar data and sleep for 60 secs to make sure we
	# get data from the beginning
	sudo rm -f /var/log/sa/*
	sleep 60
}

function dump_sar_data {

	mkdir $1/sar

	cp /var/log/sa/sa* $1/sar/

	for f in $1/sar/sa*; do

		# paging stats
		sar -f $f -B > $f.paging

		# I/O transfer rate stats
		sar -f $f -b > $f.io_transfer

		# I/O device stats
		sar -f $f -d -p > $f.devices

		# CPU stats
		sar -f $f -P ALL > $f.cpus

		# queue length
		sar -f $f -q > $f.run_queue

		# memory utilization
		sar -f $f -r ALL > $f.memory_util

		# swap
		sar -f $f -S > $f.swap

		# CPU utilization
		sar -f $f -u ALL > $f.cpu_util

		# swapping
		sar -f $f -W > $f.swapping

		# task creation
		sar -f $f -w > $f.tasks

	done

}

for s in 200 10000 20; do

	for v in 8.3 12 9.4 9.1 10 9.0 8.4 9.2 11 9.5 9.3 9.6; do

		mkdir -p $RESULTS/$s/$v

		# set the correct PATH
		PATH=/var/lib/postgresql/builds/$v/bin:$PATH_OLD

		echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "terminating old cluster"

		# kill and remove the old cluster
		killall -9 postgres > /dev/null 2>&1
		rm -Rf $DATA

		echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "initializing new cluster"

		log_step "cluster init" $RESULTS/$s/$v/steps.log

		# initialize a new cluster
		initdb -D $DATA > $RESULTS/$s/$v/init.log 2>&1

		# copy the configuration file
		cp config/$v/postgresql.conf $DATA

		# start the cluster
		pg_ctl -D $DATA -l $RESULTS/$s/$v/pg.log start > $RESULTS/$s/$v/start.log 2>&1

		# wait for the cluster to actually start
		sleep 10;

		echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "initializing pgbench database"

		log_step "pgbench init" $RESULTS/$s/$v/steps.log

		# initialize the database
		createdb test > $RESULTS/$s/$v/createdb.log 2>&1
		$PGBENCH -i -s $s test > $RESULTS/$s/$v/pgbench.init.log 2>&1

		echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "vacuum pgbench database"

		log_step "vacuum" $RESULTS/$s/$v/steps.log

		psql test -c "vacuum analyze" > $RESULTS/$s/$v/vacuum.log 2>&1

		echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "checkpoint pgbench database"

		log_step "checkpoint" $RESULTS/$s/$v/steps.log

		psql test -c "checkpoint" > $RESULTS/$s/$v/checkpoint.log 2>&1

		echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "pgbench warmup duration" $DURATION_WARMUP

		log_step "warmup" $RESULTS/$s/$v/steps.log

		psql test -c "select * from pg_settings" > $RESULTS/$s/$v/settings.log 2>&1

		# warmup
		mkdir $RESULTS/$s/$v/warmup

		# remove old sar data
		reset_sar_data

		./collect-stats.sh $RESULTS/$s/$v/warmup $v &
		stats_pid=$!

		$PGBENCH -n -S -j 4 -c 16 -T $DURATION_WARMUP -l --aggregate-interval=1 test > $RESULTS/$s/$v/warmup.log 2>&1
		mv pgbench_log.* $RESULTS/$s/$v/warmup

		kill $stats_pid > /dev/null 2>&1

		dump_sar_data $RESULTS/$s/$v/warmup

		# read-write
		for m in simple prepared; do

			for c in 1 8; do

				for r in `seq 1 2`; do

					mkdir -p $RESULTS/$s/$v/$m/$c/$r

					log_step "checkpoint" $RESULTS/$s/$v/steps.log

					psql test -c "checkpoint" > /dev/null 2>&1

					reset_sar_data

			                ./collect-stats.sh $RESULTS/$s/$v/$m/$c/$r $v &
			                stats_pid=$!

					echo [`date +'%Y/%m/%d %H:%M:%S'`] $s $v "pgbench r/w mode $m clients $c run $r duration" $DURATION_TEST

					log_step "test $m mode $c clients $r run start" $RESULTS/$s/$v/steps.log

					$PGBENCH -n -N -c $c -j $c -M $m -T $DURATION_TEST -l --aggregate-interval=1 test > $RESULTS/$s/$v/$m/$c/$r/pgbench.log 2>&1
					mv pgbench_log.* $RESULTS/$s/$v/$m/$c/$r

					log_step "test $m mode $c clients $r run done" $RESULTS/$s/$v/steps.log

					kill $stats_pid > /dev/null 2>&1

					dump_sar_data $RESULTS/$s/$v/$m/$c/$r

				done

			done

		done

		log_step "done" $RESULTS/$s/$v/steps.log

	done

done;

