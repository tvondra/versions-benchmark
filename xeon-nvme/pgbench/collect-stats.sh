#!/bin/sh

version=$2

case $version in

	10 | 11 | 12)
		fname="pg_current_wal_lsn"
		;;

	*)
		fname="pg_current_xlog_location"
		;;
esac

psql test -A -c "select extract(epoch from now()) AS epoch, now() AS ts, * from pg_stat_database where datname = 'test'" >> $1/stat-database.csv 2>&1
psql test -A -c "select extract(epoch from now()) AS epoch, now() AS ts, $fname() AS lsn, * from pg_stat_bgwriter" >> $1/stat-bgwriter.csv 2>&1

while /bin/true; do

	sleep 1

	psql test -t -A -c " select extract(epoch from now()) AS epoch, now() as ts, * from pg_stat_database where datname = 'test'" >> $1/stat-database.csv 2>&1
	psql test -t -A -c "select extract(epoch from now()) AS epoch, now() AS ts, $fname() AS lsn, * from pg_stat_bgwriter" >> $1/stat-bgwriter.csv 2>&1

done
